#!/bin/bash

killall conky
sleep 2s
		
conky -c $HOME/.config/conky/nord-theme/widgets.conf &> /dev/null &
conky -c $HOME/.config/conky/nord-theme/circle-clock.conf &> /dev/null &
